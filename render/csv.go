// Copyright 2014 Manu Martinez-Almeida.  All rights reserved.
// Use of this source code is governed by a MIT style
// license that can be found in the LICENSE file.

package render

import (
	"github.com/gocarina/gocsv"
	"net/http"
)

type (
	CSV struct {
		Filename string
		Data     interface{}
	}
)

var csvContentType = []string{"text/csv"}

func (r CSV) Render(w http.ResponseWriter) error {
	writeContentType(w, csvContentType)
	w.Header().Set("Content-Disposition", "attachment;filename="+r.Filename+".csv")
	return gocsv.Marshal(r.Data, w)
}
